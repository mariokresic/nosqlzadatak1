# Zadatak 1 - NoSQL BigData Analytics
author: mkresic1@tvz.hr @ 01.2020.

[Real estate valuation data set](https://archive.ics.uci.edu/ml/datasets/Real+estate+valuation+data+set)


> git clone git@gitlab.com:mariokresic/nosqlzadatak1.git
>
> cd nosqlzadatak1
>
> npm install
>
> npm start 
