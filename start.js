const mongo = require('mongodb');
const MongoClient = mongo.MongoClient;
const url = 'mongodb://localhost:27017';

const TYPE_KONTINUIRANA = "kontinuirana";
const TYPE_KATEGORICKA = "kategoricka";
const ATTRIBUTE_TYPES = {
  "_id": null,
  "No": null,
  "X1 transaction date": TYPE_KATEGORICKA,
  "X2 house age": TYPE_KONTINUIRANA,
  "X3 distance to the nearest MRT station": TYPE_KONTINUIRANA,
  "X4 number of convenience stores": TYPE_KONTINUIRANA,
  "X5 latitude": TYPE_KATEGORICKA,
  "X6 longitude": TYPE_KATEGORICKA,
  "Y house price of unit area": TYPE_KONTINUIRANA
}
var kategorickiAtributi = ["X1 transaction date", "X5 latitude", "X6 longitude"];
var kontinuiraniAtributi = ["X2 house age","X3 distance to the nearest MRT station","X4 number of convenience stores", "Y house price of unit area"];

async function main () {
  const client = await MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true })
    .catch(err => { console.log(err); });

  if (!client) {
    return;
  }

  try {
    const db = client.db("zadatak1");

    // import data if needed
    var collections = await db.listCollections().toArray();
    var isDataImported = collections.findIndex(c => c.name === 'csv_import') > -1;
    if (isDataImported) {
      console.log("- data already imported, skipping step");
    } else {
      // import data into new collection
      var execSync = require('child_process').execSync;
      let command = 'mongoimport -d zadatak1 -c csv_import --type csv --file data.csv --headerline';
      execSync(command);
      console.log("- importing data succesfuly");
    }

    /*
     * 1. Sve nedostajuće vrijednosti kontinuirane varijable
     *    zamijeniti sa -1, a kategoričke sa „empty“.
     */
    console.log('1. Zadatak');
    for (var attribute in ATTRIBUTE_TYPES) {
      var attributeType = ATTRIBUTE_TYPES[attribute];

      if (!attributeType) continue;

      // kontinuirana = -1, kategoricka = "empty"
      var replacementValue = attributeType === TYPE_KONTINUIRANA ? -1 : "empty";

      // update where value is nonexisting or equal to null
      await db.collection('csv_import').updateMany(
        {
          $or: [
            // where attribute doesn't exist
            { [attribute]: { $exists: false } },
            // or where attribute type is NULL
            { [attribute]: { $type: 10 } }
          ]
        },
        {
          $set: { [attribute]: replacementValue }
        }
      ).catch(err => { console.log(err); });
    } // end 1.


    /*
     * 2. Za svaku kontinuiranu vrijednost izračunati srednju
     *    vrijednost, standardnu devijaciju i kreirati novi dokument
     *    oblika sa vrijednostima, dokument nazvati:  statistika_ {ime vašeg data seta}.
     *    U izračun se uzimaju samo nomissing  vrijednosti .
     */
    console.log('2. Zadatak');
    for (var i = 0; i < kontinuiraniAtributi.length; i++) {
      var attribute = kontinuiraniAtributi[i];

      var statistika = await db.collection('csv_import').aggregate([
        {
          $match: {
            [attribute]: { $gt: -1 }
          }
        },
        {
          $group: {
            _id: attribute,
            avg: { $avg: `$${attribute}` },
            sd: { $stdDevPop: `$${attribute}` },
            nomissing: { $sum: 1 }
          }
        }
      ]).toArray();

      await db.collection('statistika_real_estate_valuation').updateOne(
        { _id: attribute },
        { $set: statistika[0] },
        { upsert: true }
      );
    } // end 2.

    /*
     * 3. Za svaku kategoričku  vrijednost izračunati frekvencije pojavnosti
     *    po obilježjima varijabli i kreirati novi dokument koristeći nizove,
     *    dokument nazvati:  frekvencija_ {ime vašeg data seta} . Frekvencije računati
     *    koristeći $inc modifikator.
     */
    console.log('3. Zadatak');
    for (var i = 0; i < kategorickiAtributi.length; i++) {
      var attribute = kategorickiAtributi[i]

      var frequencies = await db.collection('csv_import').aggregate([
        { $unwind: `$${attribute}` },
        {
          $group: {
            _id: `$${attribute}`,
            frequency: { $sum: 1 },
          }
        }
      ]).toArray()
        .catch(err => { console.log(err); });


      var frequenciesDocument = {
        _id: attribute,
        frequencies: {  }
      }

      for (var i = 0; i < frequencies.length; i++) {
        const value = frequencies[i]['_id'];
        const frequency = frequencies[i]['frequency'];
        frequenciesDocument.frequencies[value] = frequency;
      }

      await db.collection('frekvencija_real_estate_valuation').updateOne(
        { _id: attribute },
        { $set: frequenciesDocument },
        { upsert: true }
      );
    } // end 3.

    /*
     * 4. Iz osnovnog  dokumenta kreirati dva nova dokumenta sa
     *    kontinuiranim vrijednostima u kojoj će u prvom dokumentu
     *    biti sadržani svi elementi <= srednje vrijednosti , a u drugom dokumentu
     *    biti sadržani svi elementi >srednje vrijednosti , dokument nazvati:
     *    statistika1_ {ime vašeg data seta} i  statistika2_ {ime vašeg data seta} i
     */
    console.log('4. Zadatak');
    for (var i = 0; i < kontinuiraniAtributi.length; i++) {
      var attribute = kontinuiraniAtributi[i]

      var attributeStatistics = await db.collection('statistika_real_estate_valuation')
        .findOne({ _id: attribute })
      const attributeAvg = attributeStatistics['avg']

      var lessOrEqualToAverages = await db.collection('csv_import').aggregate([
        {
          $match: {
            [attribute]: { $lte: attributeAvg }
          }
        },
        {
          $group: {
            _id: null,
            values: { $push: `$${attribute}` },
          }
        }
      ]).toArray()
        .catch(err => { console.log(err); });

      var greaterThanAverages = await db.collection('csv_import').aggregate([
        {
          $match: {
            [attribute]: { $lte: attributeAvg }
          }
        },
        {
          $group: {
            _id: null,
            values: { $push: `$${attribute}` },
          }
        }
      ]).toArray()
        .catch(err => { console.log(err); });

      var lessThanAvgDocument = {
        _id: attribute,
        values: lessOrEqualToAverages[0]['values']
      };

      var grtrThanAvgDocument = {
        _id: attribute,
        values: greaterThanAverages[0]['values']
      };

      await db.collection('statistika1_real_estate_valuation').updateOne(
        { _id: attribute },
        { $set: lessThanAvgDocument },
        { upsert: true }
      );

      await db.collection('statistika2_real_estate_valuation').updateOne(
        { _id: attribute },
        { $set: grtrThanAvgDocument },
        { upsert: true }
      );
    } // end 4.

    /*
     * 5. Osnovni  dokument  kopirati u novi te embedati vrijednosti
     *    iz tablice 3 za svaku kategoričku vrijednost, :  emb_ {ime vašeg data seta}
     */
    console.log('5. Zadatak');
    var documentsToEmbed1 = await db.collection('csv_import').find({}).toArray();
    var dataToEmbed1 = await db.collection('frekvencija_real_estate_valuation').find({}).toArray();
    for (var i = 0; i < documentsToEmbed1.length; i++) {
      var document = documentsToEmbed1[i];

      for (var y = 0; y < dataToEmbed1.length; y++) {
        const attribute = dataToEmbed1[y]['_id'];
        const frequencies = dataToEmbed1[y]['frequencies'];
        const attributeName = attribute + " frequencies";

        document[attributeName] = frequencies;
      }

      await db.collection('emb_real_estate_valuation').updateOne(
        { _id: document['_id'] },
        { $set: document },
        { upsert: true }
      );
    } // end 5.


    /*
     * 6. Osnovni  dokument  kopirati u novi te embedati vrijednosti iz tablice 2 za
     *    svaku kontinuiranu  vrijednost kao niz :  emb2_ {ime vašeg data seta}
     */
    console.log('6. Zadatak');
    var documentsToEmbed2 = await db.collection('csv_import').find({}).toArray();
    var dataToEmbed2 = await db.collection('statistika_real_estate_valuation').find({}).toArray();
    for (var i = 0; i < documentsToEmbed2.length; i++) {
      var document = documentsToEmbed2[i];

      for (var j = 0; j < dataToEmbed2.length; j++) {
        let attributeName = dataToEmbed2[j]['_id'] + " statistics";
        let avg = {
          id: 'avg',
          val: dataToEmbed2[j]['avg']
        };
        let nomissing = {
          id: 'nomissing',
          val: dataToEmbed2[j]['nomissing']
        };
        let sd = {
          id: 'sd',
          val: dataToEmbed2[j]['sd']
        };

        document[attributeName] = [avg, nomissing, sd];
      }

      await db.collection('emb2_real_estate_valuation').updateOne(
        { _id: document['_id'] },
        { $set: document },
        { upsert: true }
      );
    } // end 6.


    /*
     * 7. Iz tablice emb2 izvući sve one srednje vrijednosti  iz nizova čija je
     *    standardna devijacija 10% > srednje vrijednosti
     *    koristeći $set modifikator
     */
    console.log('7. Zadatak');
    var documents7 = await db.collection('emb2_real_estate_valuation').find({}).toArray();
    for (let i = 0; i < documents7.length; i++) {
      let document = documents7[i];


      for (let j = 0; j < kontinuiraniAtributi.length; j++) {
        let attribute = kontinuiraniAtributi[j] + " statistics";

        let sd = document[attribute].find(s => s.id === 'sd');
        let avg = document[attribute].find(s => s.id === 'avg');
        let nomissing = document[attribute].find(s => s.id === 'nomissing');


        if (sd.val > (avg.val * 1.1)) {
          let newStatisticsArray = [sd, nomissing];

          await db.collection('emb2_real_estate_valuation').updateOne(
            { _id: document['_id'] },
            { $set: { [attribute]: newStatisticsArray } }
          );
        }
      }
    } // end 7.

    /*
     * 8. Kreirati složeni indeks na originalnoj tablici i
     *    osmisliti upit koji je kompatibilan sa indeksom
     */
    console.log('8. Zadatak');
    // creates compound index
    await db.collection('csv_import').createIndex({ "X4 number of convenience stores" : 1, "X3 distance to the nearest MRT station": 1 }, { name: "slozeni_index" });
    var indexQueryResult = await db.collection('csv_import').find({ "X2 house age": 0 }).sort( { "X4 number of convenience stores": -1, "X3 distance to the nearest MRT station": 1 } ).toArray();
    // check indexes if needed
    // var indexes = await db.collection('csv_import').indexes();
    // console.log(indexQueryResult);
  } catch (err) {

    console.log(err);
  } finally {

    client.close();
  }
}

main();
